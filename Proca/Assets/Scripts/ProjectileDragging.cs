﻿using UnityEngine;

public class ProjectileDragging : MonoBehaviour {
	
	public float maxStretch = 3.0f;
	public float shotPower = 10;
	public LineRenderer catapultLineFront;
	public LineRenderer catapultLineBack;

	public GameObject explosionPrefab;

	public Transform spawnPlace;

	GameObject explosion;

	Ray rayToMouse;
	Ray leftCatapultToProjectile;

	float maxStretchSqr;
	float circleRadius;

	float explosionTimeAfterRelease = 1;

	bool stretched;

	Rigidbody2D rb;

	Vector2 prevVelocity;

	SpringJoint2D spring;

	Transform catapult;
	
	
	void Awake () {
		spring = GetComponent <SpringJoint2D> ();
		rb = GetComponent <Rigidbody2D> ();
		catapult = spring.connectedBody.transform;
	}

	void Start () {
		SetupTheLineRenderer ();
		rayToMouse = new Ray(catapult.position, Vector3.zero);
		leftCatapultToProjectile = new Ray(catapultLineFront.transform.position, Vector3.zero);
		maxStretchSqr = maxStretch * maxStretch;
		CircleCollider2D circle = GetComponent<CircleCollider2D> ();
		circleRadius = circle.radius;
	}
	
	void Update () {
		if (stretched)
			Dragging ();

		//If the spring is enabled.
		if (spring.enabled == true) {
			//If the rigidbody is not kinematic and the square magnitude of the previous velocity is bigger than square magnitude of current velocity.
			if (!rb.isKinematic && prevVelocity.sqrMagnitude > rb.velocity.sqrMagnitude) {
				//Invokes the explode method, turns off the spring and sets the velocity of the projectile.
				Invoke("Explode", explosionTimeAfterRelease);
				spring.enabled = false;
				rb.velocity = prevVelocity * shotPower;
			}
			
			if (!stretched) {
				//Calculates the velocity before shot.
				prevVelocity = rb.velocity;
			}

			//Update the line graphics.
			LineRendererUpdate ();
			
		} else {
			//Turns off the lines after the spring deactivation.
			catapultLineFront.enabled = false;
			catapultLineBack.enabled = false;
		}
	}

	/// <summary>
	/// Sets up the line renderer position and sorting order.
	/// </summary>
	void SetupTheLineRenderer () {
		catapultLineFront.SetPosition(0, catapultLineFront.transform.position);
		catapultLineBack.SetPosition(0, catapultLineBack.transform.position);

		catapultLineFront.sortingOrder = 3;
		catapultLineBack.sortingOrder = 1;
	}

	void OnMouseDown () {
		//Sets streched to true if the projectile was clicked.
		stretched = true;
	}
	
	void OnMouseUp () {
		//Sets streched to false and the rigidbody is no longer kinematic.
		stretched = false;
		rb.isKinematic = false;
	}

	/// <summary>
	/// Projectiles follows the cursor to the maximum band stretch.
	/// </summary>
	void Dragging () {
		Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector2 catapultToMouse = mouseWorldPoint - catapult.position;
		
		if (catapultToMouse.sqrMagnitude > maxStretchSqr) {
			rayToMouse.direction = catapultToMouse;
			mouseWorldPoint = rayToMouse.GetPoint(maxStretch);
		}
		
		mouseWorldPoint.z = 0f;
		transform.position = mouseWorldPoint;
	}

	/// <summary>
	/// Updates the lines graphics by current projectile position.
	/// </summary>
	void LineRendererUpdate () {
		Vector2 catapultToProjectile = transform.position - catapultLineFront.transform.position;
		leftCatapultToProjectile.direction = catapultToProjectile;
		Vector3 holdPoint = leftCatapultToProjectile.GetPoint(catapultToProjectile.magnitude + circleRadius);
		catapultLineFront.SetPosition(1, holdPoint);
		catapultLineBack.SetPosition(1, holdPoint);
	}

	/// <summary>
	/// Makes the rigidbody kinematic again, spawns an explosion, sets the projectile position to spawn position, activates the spring and invokes destroyFX method.
	/// </summary>
	void Explode ()
	{
		rb.isKinematic = true;
		explosion = Instantiate (explosionPrefab, transform.position, transform.rotation) as GameObject;
		transform.position = spawnPlace.position;
		spring.enabled = true;

		Invoke ("DestroyFX", explosionTimeAfterRelease);
	}

	/// <summary>
	/// Destroys the FX.
	/// </summary>
	void DestroyFX ()
	{
		Destroy (explosion);
	}
}
