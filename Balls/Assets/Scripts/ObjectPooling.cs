﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooling : MonoBehaviour {

	public GameObject ballPrefab;

	GameObject ball;

	List<GameObject> ballsList;

	void Awake ()
	{
		ballsList = new List<GameObject>();
	}

	/// <summary>
	/// Instantiates a new ball, adds it to the list and returns it.
	/// </summary>
	/// <returns>The ball.</returns>
	public GameObject GetBall ()
	{
		ball = Instantiate (ballPrefab);
		ballsList.Add (ball);
		return ball;
	}

	/// <summary>
	/// Returns an inactive ball from the pool.
	/// </summary>
	/// <returns>The pooled ball.</returns>
	public GameObject GetPooledBall ()
	{
		foreach(GameObject bal in ballsList)
		{
			if(!bal.activeInHierarchy)
			{
				return bal;
			}
		}

		return ball;
	}
}
