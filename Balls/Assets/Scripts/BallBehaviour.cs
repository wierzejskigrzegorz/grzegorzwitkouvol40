﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour {

	int maximumMass = 50;

	float explosionForce = 3000;

	float notCollidingTime = 0.5f;

	bool willExplode = false;
	bool merge = true;

	Rigidbody2D rb;

	CircleCollider2D circlCol;

	BallsCounter ballsCounter;
	BallSpawner ballSpawner;

	void Awake ()
	{
		rb = GetComponent<Rigidbody2D> ();
		circlCol = GetComponent<CircleCollider2D> ();
		ballSpawner = FindObjectOfType<BallSpawner> ();
		ballsCounter = FindObjectOfType<BallsCounter> ();
	}

	void Start ()
	{
		ballsCounter.AddBall ();
	}

	void OnEnable ()
	{
		//If the ball will explode it adds a random force and TurnsOnTheCollision after a given time.
		if(willExplode)
		{
			AddRandomForce ();
			Invoke ("TurnOnTheCollision", notCollidingTime);
		}
	}

	void OnDisable ()
	{
		//If the object was disabled it will explode and its collider is deactivated.
		willExplode = true;
		circlCol.enabled = false;
	}

	void OnCollisionStay2D (Collision2D other)
	{
		//If balls are able to merge
		if (merge)
		{
			//If the mass of the ball rigidbody is bigger than the mass of collision ball or if those massess are equal but the ball position x is bigger than the other ball.
			if((rb.mass > other.rigidbody.mass) || rb.mass == other.rigidbody.mass && (transform.position.x > other.transform.position.x))
			{
				//Adds the other balls mass to this ball mass and sets the scale of this ball same as mass.
				rb.mass += other.rigidbody.mass;
				transform.localScale = new Vector2 (rb.mass, rb.mass);

				//Turns off the other ball.
				other.gameObject.SetActive (false);

				//If the mass reached the maximum mass its activates the Explosion, resets values to default and turns of the object.
				if(rb.mass >= maximumMass)
				{
					ballSpawner.Explosion (rb.mass, transform.position);
					ResetValuesToDefault ();
					gameObject.SetActive (false);
				}
			}
		}					
	}

	/// <summary>
	/// Resets the value of mass and scale to default.
	/// </summary>
	void ResetValuesToDefault()
	{
		rb.mass = 1;
		transform.localScale = new Vector2(rb.mass, rb.mass);
	}

	/// <summary>
	/// Adds random force to the rigidbody.
	/// </summary>
	void AddRandomForce()
	{
		rb.AddForce (Random.onUnitSphere * explosionForce);
	}

	/// <summary>
	/// Turns on the collision.
	/// </summary>
	void TurnOnTheCollision ()
	{
		Debug.Log ("tak");
		circlCol.enabled = true;
	}

	/// <summary>
	/// Stops the balls from merging.
	/// </summary>
	public void StopMerging ()
	{
		merge = false;
		rb.drag = 1;
	}
}
