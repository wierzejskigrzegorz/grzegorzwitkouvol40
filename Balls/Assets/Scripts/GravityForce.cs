﻿using UnityEngine;
using System.Collections;

public class GravityForce : MonoBehaviour {

	public static float force = 10;

	Vector2 direction;

	void OnTriggerStay2D (Collider2D other)
	{
		//Calculates the direction in which the force should be added, and adds the force to the other rigidbody.
		direction = -(other.attachedRigidbody.transform.position - transform.position);

		other.attachedRigidbody.AddForce (direction * force);
	}
}
