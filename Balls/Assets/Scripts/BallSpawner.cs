﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour {
	
	float spawnTime = 0.25f;

	GameObject ball;

	ObjectPooling objectPooling;

	void Awake ()
	{
		InvokeRepeating ("SpawnBallAtRandom", spawnTime, spawnTime);
		objectPooling = FindObjectOfType<ObjectPooling> ();
	}

	/// <summary>
	/// Spawns the ball at random position on the screen.
	/// </summary>
	void SpawnBallAtRandom ()
	{
		Vector2 randomPositionOnTheScreen = Camera.main.ScreenToWorldPoint (new Vector2 (Random.Range (0, Screen.width), Random.Range (0, Screen.height)));

		ball = objectPooling.GetBall ();
		ball.transform.position = randomPositionOnTheScreen;
		ball.SetActive (true);
	}

	/// <summary>
	/// Spawns as much balls as the value of ballsToSpawn param on the given position in param spawnPosition.
	/// </summary>
	/// <param name="ballsToSpawn">Balls to spawn.</param>
	/// <param name="spawnPosition">Spawn position.</param>
	public void Explosion (float ballsToSpawn, Vector2 spawnPosition)
	{
		for (int i = 0; i < ballsToSpawn; i++) {
			ball = objectPooling.GetPooledBall ();
			ball.transform.position = spawnPosition;
			ball.SetActive (true);
		}
	}

	/// <summary>
	/// Stops spawning balls.
	/// </summary>
	public void StopSpawning ()
	{
		CancelInvoke ();
	}
}
