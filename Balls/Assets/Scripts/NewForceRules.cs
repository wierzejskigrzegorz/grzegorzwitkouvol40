﻿using UnityEngine;
using System.Collections;

public class NewForceRules : MonoBehaviour {

	BallBehaviour [] ballBe;

	BallSpawner ballSpawner;

	void Awake ()
	{
		ballSpawner = FindObjectOfType<BallSpawner> ();
	}

	/// <summary>
	/// Prevents balls from spawning, merging, and makes them push themselfs instead of pulling.
	/// </summary>
	public void ChangeRules ()
	{
		ballBe = FindObjectsOfType <BallBehaviour> ();
		ballSpawner.StopSpawning ();

		GravityForce.force = -GravityForce.force;

		for (int i = 0; i < ballBe.Length; i++)
		{
			ballBe [i].StopMerging ();
		}
	}
}
