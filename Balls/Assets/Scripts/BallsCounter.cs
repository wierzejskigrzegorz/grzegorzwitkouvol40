﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallsCounter : MonoBehaviour {

	int ballsCount;
	int maximumBallsCount = 250;

	Text counterText;

	NewForceRules newForceRules;

	void Awake ()
	{
		counterText = GetComponent<Text> ();
		newForceRules = FindObjectOfType<NewForceRules> ();
	}

	/// <summary>
	/// Sets the UI.
	/// </summary>
	void SetUI ()
	{
		counterText.text = "Balls: " + ballsCount;
	}

	/// <summary>
	/// Adds 1 to the total amount of balls and sets the UI.
	/// </summary>
	public void AddBall ()
	{
		ballsCount++;

		//If the amount of balls reaches maximum, it Changes the rules.
		if (ballsCount >= maximumBallsCount)
			newForceRules.ChangeRules ();
		
		SetUI ();
	}
}
